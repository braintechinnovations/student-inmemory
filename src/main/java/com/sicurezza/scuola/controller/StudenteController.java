package com.sicurezza.scuola.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sicurezza.scuola.model.Studente;
import com.sicurezza.scuola.service.StudenteService;

@CrossOrigin("http://localhost:8080")
@RestController
public class StudenteController {

	@Autowired
	private StudenteService studRepo;
	
	@GetMapping("/studenti")
	public ResponseEntity<List<Studente>> getAllStudenti(){
		
		List<Studente> lista_temporanea = studRepo.retrieveAllStudenti();
		
		if(!lista_temporanea.isEmpty()) {
			return new ResponseEntity<List<Studente>>(lista_temporanea, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
}
