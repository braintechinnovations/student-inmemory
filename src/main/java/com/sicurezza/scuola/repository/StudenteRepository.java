package com.sicurezza.scuola.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sicurezza.scuola.model.Corso;
import com.sicurezza.scuola.model.Studente;

@Repository
public interface StudenteRepository {
	
	Studente retrieveStudente(long StudenteId);
	List<Studente> retrieveAllStudenti();
	List<Corso> retrieveCorsi(long StudenteId);

}
