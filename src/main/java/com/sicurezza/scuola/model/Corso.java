package com.sicurezza.scuola.model;

import java.util.List;

public class Corso {
	
	//Variables
	private long id;
	private String course_name;
	private int course_cred;
	private String course_desc;
	private List<String> course_doce;
	
	//Constructor
	public Corso() {
		
	}
	
	public Corso(long id, String course_name, int course_cred, String course_desc, List<String> course_doce) {
		super();
		this.id = id;
		this.course_name = course_name;
		this.course_cred = course_cred;
		this.course_desc = course_desc;
		this.course_doce = course_doce;
	}
	
	//Methods
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public int getCourse_cred() {
		return course_cred;
	}
	public void setCourse_cred(int course_cred) {
		this.course_cred = course_cred;
	}
	public String getCourse_desc() {
		return course_desc;
	}
	public void setCourse_desc(String course_desc) {
		this.course_desc = course_desc;
	}
	public List<String> getCourse_doce() {
		return course_doce;
	}
	public void setCourse_doce(List<String> course_doce) {
		this.course_doce = course_doce;
	}
	
}
