package com.sicurezza.scuola.model;

import java.util.List;

public class Studente {

	private long id;
	private String stud_nomi;
	private String stud_matr;
	private List<Corso> stud_cors;
	
	public Studente() {
		
	}
	
	public Studente(long id, String stud_nomi, String stud_matr, List<Corso> stud_cors) {
		super();
		this.id = id;
		this.stud_nomi = stud_nomi;
		this.stud_matr = stud_matr;
		this.stud_cors = stud_cors;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStud_nomi() {
		return stud_nomi;
	}
	public void setStud_nomi(String stud_nomi) {
		this.stud_nomi = stud_nomi;
	}
	public String getStud_matr() {
		return stud_matr;
	}
	public void setStud_matr(String stud_matr) {
		this.stud_matr = stud_matr;
	}
	public List<Corso> getStud_cors() {
		return stud_cors;
	}
	public void setStud_cors(List<Corso> stud_cors) {
		this.stud_cors = stud_cors;
	}
}
