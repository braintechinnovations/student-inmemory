package com.sicurezza.scuola.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	//Assegnamo all'utente la ROLE
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("ciccio").password("{noop}pasticcio").roles("USER")
			.and()
			.withUser("administrator").password("{noop}1234").roles("ADMIN", "USER");
	}
	
	//Verifico che la path corrisponda alla Role
	protected void configure(HttpSecurity http) throws Exception {
		http
		.httpBasic()
		.and()
		.authorizeRequests()
		.antMatchers("/studenti/**")
		.hasRole("USER")
		.antMatchers("/corsi/**")
		.hasRole("ADMIN")
		.and()
		.csrf().disable().headers().frameOptions().disable();
	}

}
