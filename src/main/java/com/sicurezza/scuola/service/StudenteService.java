package com.sicurezza.scuola.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sicurezza.scuola.model.Corso;
import com.sicurezza.scuola.model.Studente;
import com.sicurezza.scuola.repository.StudenteRepository;

@Service
public class StudenteService implements StudenteRepository{

	private static List<Studente> lista_studenti = new ArrayList<>();
	
	static {
		Corso corso_fisica = new Corso(1, "Fisica Generale", 6, "Un corso davvero tanto triste!", Arrays.asList("Beatrice", "Karma"));
		Corso corso_informatica = new Corso(2, "Informatica I", 9, "Un corso davvero tanto eccitante!", Arrays.asList("Giovanni", "Mirko", "Marco"));
		
		Studente greta = new Studente(1, "Greta", "AB1234", Arrays.asList(corso_fisica, corso_informatica));
		
		lista_studenti.add(greta);
	}
	
	@Override
	public Studente retrieveStudente(long StudenteId) {
		
		for(Studente stud: lista_studenti) {
			if(stud.getId() == StudenteId) {
				return stud;
			}
		}
		
		return null;
	}

	@Override
	public List<Corso> retrieveCorsi(long StudenteId) {
		
		Studente temp = retrieveStudente(StudenteId);
		
		if(temp == null) {
			return null;
		}
		
		return temp.getStud_cors();
	}
	
	@Override
	public List<Studente> retrieveAllStudenti(){
		return lista_studenti;
	}

	
	
}
